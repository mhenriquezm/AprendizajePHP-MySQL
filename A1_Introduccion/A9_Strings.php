<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Uso de string</title>

	<style>
		.resaltar {color:#f00; font-weight:bold;}
	</style>
</head>
<body>
	<?php
		/*Para usar comillas anidadas se deben intercalar entre 
		simples y dobles o dobles y simples. También se puede hacer
		lo que se conoce como escape de caracteres (\") esto significa
		que dicho caracter no forma parte del string*/
		echo "<p class=\"resaltar\">Esto es un ejemplo de frase</p>";
	?>
</body>
</html>