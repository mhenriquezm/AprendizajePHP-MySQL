<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Ámbito de las variables</title>
</head>
<body>
	<?php
		$nombre = "Manuel";
		
		function devolverNombre() {
			/*Cambiar el ámbito de la variable, para ello usamos la
			instrucción global y hacemos global la variable que esta
			fuera de la función (debe colocarse siempre dentro de la
			función)*/
			global $nombre;

			$nombre = "Su nombre es: " . $nombre;
		}

		devolverNombre();

		echo $nombre;//Imprime Su nombre es: Manuel
	?>
</body>
</html>