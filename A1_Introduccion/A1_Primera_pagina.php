<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Primera página</title>
</head>
<body>
	<?php
	    //Ésta línea imprime un mensaje 
		print("¡Hola mundo, ésta es mi primera página PHP!");
		print("<br/>Aprenderemos mucho en éste curso");
		print("<br/>Hasta la próxima");

		/*Se pueden incluir etiquetas html en el código php
		(<br> es un salto de línea)*/
	?>
</body>
</html>