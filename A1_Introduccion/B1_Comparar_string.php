<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Comparar cadenas</title>
</head>
<body>
	<?php
		$variable1 = "Casa";
		$variable2 = "CASA";

		//Comparar cadenas tomando en cuenta mayúsculas y minúsculas
		$resultado = strcmp($variable1, $variable2);

		//Comparar cadenas sin tomar en cuenta mayúsculas y minúsculas
		$resultado2 = strcasecmp($variable1, $variable2);

		/*Retorna 1 si no son iguales y 0 si son iguales*/
		echo "El resultado es: " . $resultado . ". No coinciden<br>";
		echo "El resultado es: " . $resultado2 . ". Coinciden<br>";
	?>
</body>
</html>