<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Flujo de ejecución</title>
</head>
<body>
	<?php
		/*Podemos aislar funciones en documentos php externos para
		posteriormente incluirlas en otros documentos a través de las
		funciones include o require. Ambas funciones permiten incluir
		archivos externos en el documento, pero la diferencia radica
		en que require lanza una excepción si el documento no existe
		debido a que es requerido y detiene el flujo de ejecución,
		include muestra el error, pero el flujo de ejecución no se 
		detiene*/
		
		include("A5_Flujo_ejecucion.php");
		//require("A5_Flujo_ejecucion.php");

		echo "Este es el primer mensaje <br>";
		echo "Este es el segundo mensaje <br>";

		//El código de una función no se ejecuta hasta que se llama
		devolverDatos();
	?>
</body>
</html>