<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Comentarios y variables</title>
</head>
<body>
	<?php
		//Esto es un comentario de una línea
	    
	    /*Esto es un comentario de varias líneas, funciones:
	        - Invalidar código
	        - Crear anotaciones o guías personales o para otros
	        - No se toman en cuenta a la hora de interpretar la web
	    */
	    
	    /*Variable: espacio en la memoria (RAM) donde se almacenará 
	    un valor que puede cambiar durante la ejecución del programa.

	    Reglas al usar variables:
	    	- No usar caracteres latinos, ni espacios en blanco
	    	- No pueden iniciar su nombre con un número, pero si puede
	    	tener números en su nombre.
	    	- Comienzan con el símbolo $
	    */
	    
	    $nombre = "Manuel";//Declarar e iniciar una variable
	    $edad = 26;//PHP es flexible con el tipo de datos

	    //Para concatenar en PHP se utiliza el punto .
	    print "- Nombre: $nombre<br>- Edad: $edad";

	    /*Si usamos comillas dobles podemos incluir una variable
	    dentro de dichas comillas sin concatenar y se imprimirá el
	    valor de la variable, al usar comillas simples se imprimirá 
	    el literal (nombre de la variable)*/

	    //print '- Nombre: $nombre<br>- Edad: ' . $edad;
	?>
</body>
</html>