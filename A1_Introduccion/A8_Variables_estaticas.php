<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Variables estáticas</title>
</head>
<body>
	<?php
		function incrementarVariable() {
			/*Al declarar una variable estática será compartida por
			todas las llamadas a la función y su valor se conserva*/
			static $contador = 0;

			$contador++;//$contador = $contador + 1;

			echo "Valor del contador: " . $contador . "<br>";

			/*Al crear variables dentro de funciones su ámbito es
			local, es decir, su zona de acción es desde la llave de 
			apertura hasta la llave de cierre de la función, pero 
			además al finalizar la función esas variables son 
			destruidas*/
		}

		incrementarVariable();
		incrementarVariable();
		incrementarVariable();
		incrementarVariable();
	?>
</body>
</html>