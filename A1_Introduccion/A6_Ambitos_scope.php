<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Ámbito de las variables</title>
</head>
<body>
	<?php
		$nombre = "Manuel";

		/*El ámbito de una función en PHP está muy diferenciado del
		ámbito del resto del programa, tanto es así que si declaramos
		dos variables con el mismo nombre dentro y fuera de una 
		funcion, para PHP son variables diferentes, esto es asi por
		la naturaleza de PHP de poder incluir archivos externos a 
		traves de include y require, al diferenciar los ámbitos 
		tenemos seguridad de sobreescritura de variables*/
		
		function devolverNombre() {
			$nombre = "Raquel";
		}

		devolverNombre();

		echo $nombre;//Imprime Manuel
	?>
</body>
</html>