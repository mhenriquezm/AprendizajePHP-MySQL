<?php
	/*El flujo de ejecución de un programa o aplicación en 
	principio va de arriba hacia abajo, es decir, las etiquetas
	HTML, estilos CSS, archivos JavaScript, archivos PHP, etc. 
	A no ser que se modifique se interpretan en orden de arriba 
	hacia abajo y de izquierda a derecha. Hay que tener esto
	en cuenta debido a que podemos tener varios bloques php en
	el mismo documento web. Existen estructuras de control de 
	flujo o declaraciones como:
		1. Condicionales
		2. Bucles
		3. Funciones
	*/
	
	function devolverDatos() {
		/*La zona de acción o ámbito de una función va desde su
		llave de apertura hasta su llave de cierre*/
		echo "Este es el mensaje del interior de la función <br>";
	}
?>