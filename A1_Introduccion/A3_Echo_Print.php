<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>echo y print</title>
</head>
<body>
	<?php
		$nombre = "Manuel";
		$edad = 26;

		/*En principio echo y print imprimen texto y son similares
		*/
	
		echo "- Nombre: " . $nombre . "<br>- Edad: " . $edad;

		echo "<br><br>";

		print "- Nombre: " . $nombre . "<br>- Edad: " . $edad;

		echo "<br><br>";

		echo $nombre,$edad;//Permitido
		//print $nombre,$edad;//No permitido
		
		/*La diferencia fundamental entre print y echo es que echo
		es una expresión y print una función getter de retorno 1
		al ser una función tiene más procesos internos y consume más
		recursos, por eso es mejor utilizar echo*/
	?>
</body>
</html>