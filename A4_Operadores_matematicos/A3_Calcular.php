<style>
    .resultado {
        color: #00f;
        font-weight: bold;
        font-size: 32px;
    }
</style>

<?php
    function calcular($operacion) {

        global $num1;

        if(!strcmp("Incrementar 1", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " = " . (++$num1) 
            . "</p>";
        }
        else if(!strcmp("Decrementar 1", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " = " . (--$num1)
            . "</p>";
        }
    }
?>