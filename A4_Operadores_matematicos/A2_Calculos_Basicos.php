<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Cálculos matemáticos</title>
</head>
<body>
    <p>&nbsp;</p>

    <form name="form1" method="POST" action="">
        <p>
            <input type="text" name="num1" id="num1">

            <select name="operacion" id="operacion">
                <option>Incrementar 1</option>
                <option>Decrementar 1</option>
            </select>
        </p>

        <p>
            <input type="submit" name="boton" id="boton" value="Enviar">
        </p>
    </form>

    <p>&nbsp;</p>

    <?php
        //Incluir la función que procesa los datos
        include("A3_Calcular.php");

        if(isset($_POST["boton"])) {

            $num1 = $_POST["num1"];
            $operacion = $_POST["operacion"];

            //Llamar a la función previamente incluida
            calcular($operacion);
        }
    ?>
</body>
</html>