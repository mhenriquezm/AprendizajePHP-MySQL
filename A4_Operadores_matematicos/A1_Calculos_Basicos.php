<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Cálculos matemáticos</title>
</head>
<body>
    <p>&nbsp;</p>

    <form name="form1" method="POST" action="">
        <p>
            <input type="text" name="num1" id="num1">
            <input type="text" name="num2" id="num2">

            <select name="operacion" id="operacion">
                <option>Suma</option>
                <option>Resta</option>
                <option>Multiplicación</option>
                <option>División</option>
                <option>Módulo</option>
            </select>
        </p>

        <p>
            <input type="submit" name="boton" id="boton" value="Enviar">
        </p>
    </form>

    <p>&nbsp;</p>

    <?php
        if(isset($_POST["boton"])) {

            $num1 = $_POST["num1"];
            $num2 = $_POST["num2"];
            $operacion = $_POST["operacion"];

            if(!strcmp("Suma", $operacion)) {
                
                echo "Resultado: " . $num1 . " + " . $num2 . " = " 
                    . ($num1 + $num2);
            }
            else if(!strcmp("Resta", $operacion)) {
                
                echo "Resultado: " . $num1 . " - " . $num2 . " = " 
                    . ($num1 - $num2);
            }
            else if(!strcmp("Multiplicación", $operacion)) {
                
                echo "Resultado: " . $num1 . " * " . $num2 . " = " 
                    . ($num1 * $num2);
            }
            else if(!strcmp("División", $operacion)) {
                
                echo "Resultado: " . $num1 . " / " . $num2 . " = " 
                    . ($num1 / $num2);
            }
            else if(!strcmp("Módulo", $operacion)) {
                
                echo "Resultado: " . $num1 . " Mod " . $num2 . " = " 
                    . ($num1 % $num2);
            }
        }
    ?>
</body>
</html>