<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Casting o refundición</title>
</head>
<body>
	<?php
		/*PHP al contratio de otros lenguajes de programación tiene
		una asignación de tipos dinámica o según su contexto, lo que
		nos lleva al concepto de refundición o casting implícito*/

		$num1 = "5";

		echo "El tipo de dato es: " . gettype($num1);//Tipo string
		echo "<br>";

		$num1 = 5;

		echo "El tipo de dato es: " . gettype($num1);//Tipo integer
		echo "<br>";

		$num1 = 5.7556454;

		echo "El tipo de dato es: " . gettype($num1);//Tipo double
		echo "<br>";

		//Casting explícito
		$num1 = "5";
		$resultado = (int) $num1;//Convertir a entero un string
	?>
</body>
</html>