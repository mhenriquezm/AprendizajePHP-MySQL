<style>
    .resultado {
        color: #00f;
        font-weight: bold;
        font-size: 32px;
    }
</style>

<?php
    //Si el documento php solo procesará información no necesita HTML
    function calcular($operacion) {
        
        global $num1, $num2;/*Combinar el paso de parámetros con variables globales*/

        if(!strcmp("Suma", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " + " . $num2 . " = " . ($num1 + $num2) 
            . "</p>";
        }
        else if(!strcmp("Resta", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " - " . $num2 . " = " . ($num1 - $num2) 
            . "</p>";
        }
        else if(!strcmp("Multiplicación", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " * " . $num2 . " = " . ($num1 * $num2) 
            . "</p>";
        }
        else if(!strcmp("División", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " / " . $num2 . " = " . ($num1 / $num2) 
            . "</p>";
        }
        else if(!strcmp("Módulo", $operacion)) {
            
            echo "<p class='resultado'>Resultado: " 
                . $num1 . " Mod " . $num2 . " = " . ($num1 % $num2) 
            . "</p>";
        }
    }

    if(isset($_POST["boton"])) {

        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $operacion = $_POST["operacion"];

        //Podemos pasarle parámetros a la función para su uso local
        calcular($operacion);
    }
?>