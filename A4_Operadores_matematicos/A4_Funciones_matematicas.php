<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Funciones matemáticas</title>
</head>
<body>
	<?php
		$potencia = pow(5, 3);
		$redondeo = 5.2542;

		echo "Resultado 5 ^ 3 = " . $potencia;
		echo "<br>";
		echo "Redeondeo: " . $redondeo . " = " 
		. round($redondeo, 2, PHP_ROUND_HALF_UP);

		//Redondeo decimal con precición de 2 y hacia arriba
	?>
</body>
</html>