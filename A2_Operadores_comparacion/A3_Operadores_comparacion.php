<style>
	.no-validado {
		font-size: 18px;
		font-weight: bold;
		color: #f00;
	}

	.validado {
		font-size: 18px;
		font-weight: bold;
		color: #0c3;
	}
</style>

<?php
	/*Haciendo uso de la variable super global $_POST["var"]
	podemos acceder a las variables enviadas por un formulario
	estas variables o campos deben tener definido su atributo
	name que sera el parametro dentro de los corchetes para 
	poder referenciarlo, también se puede usar $_GET["var"],
	pero eso depende del método de envío. La función isset
	recibe una variable y verifica si tiene un valor*/

	//Comprobar si el botón fue pulsado o tiene valor
	if(isset($_POST["enviar"])) {

		//Almacenamos los datos del usuario
		$nombre = $_POST["nombre"];
		$edad = $_POST["edad"];

		if(($nombre == "Manuel") && ($edad >= 18)) {

			echo "<p class='validado'>Puedes entrar</p>";
		}
		else {

			echo "<p class=\"no-validado\">" 
				. "No puedes entrar" 
			. "</p>";
		}
	}
?>