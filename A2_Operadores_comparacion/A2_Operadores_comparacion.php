<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Operadores de comparación</title>

	<style>
		header {
			width: 800px;
			margin: 0px auto;
			text-transform: uppercase;
		}

		header > h1 {
			text-align: center;
		}

		section {
			width: 800px;
			margin: 0px auto;
			position: relative;
		}

		section > form table {
			border: 5px solid #666;
			margin: 0px auto;
			padding: 5px;
			background: #FFC;
		}
	</style>
</head>
<body>
	<header>
		<h1>Usando operadores de comparación</h1>
	</header>

	<section>
		<!-- El atributo action comunica el documento actual con otro documento php -->
		<form action="A3_Operadores_comparacion.php" method="POST" name="registro" id="registro">
			<table>
				<tr>
					<td><label for="nombre">Nombre:</label></td>
					<td>
						<input type="text" name="nombre" id="nombre">
					</td>
				</tr>
		
				<tr>
					<td><label for="edad">Edad:</label></td>
					<td>
						<input type="text" name="edad" id="edad">
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="enviar" id="enviar" value="Enviar">
					</td>
				</tr>
			</table>
		</form>
	</section>
</body>
</html>