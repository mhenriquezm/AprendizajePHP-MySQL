<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Operadores de comparación</title>
</head>
<body>
	<?php
		$variable1 = 8;
		$variable2 = '8';

		if($variable1 === $variable2) {
			echo "Son iguales";
		}
		else {
			//Tienen el mismo valor pero distintos tipos
			echo "No son iguales";
		}
	?>
</body>
</html>