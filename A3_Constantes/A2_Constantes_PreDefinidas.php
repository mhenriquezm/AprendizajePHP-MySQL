<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Constantes pre-definidas</title>
</head>
<body>
	<?php
		//Muestra la versión de php actual
		echo "La versión de PHP que estás usando es: " . PHP_VERSION;

		//Constantes mágicas, se les da valor en tiempo de compilación
		echo "<br>Ruta del fichero: " . __FILE__;
	?>
</body>
</html>