<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Uso de constantes</title>
</head>
<body>
	<?php
		/*Define nos permite crear una constante, recibe 3 parámetros:
			1. Nombre de la constante: string y obligatorio, se 
			nombran en mayúsculas por convención
			2. Valor de la constante: mixed y obligatorio
			3. Sensible a mayúsculas y minúsculas: booleano y opcional
		*/
		define("AUTOR", "Manuel Henriquez");

		//Las constantes son llamadas sin utilizar el símbolo $
		echo "El autor de el documento es: " . AUTOR;
	?>
</body>
</html>